# UberTruck
## Uber-like system for freight trucks
The system for delivery with freight trucks.  
Implemented with `express`, `mongodb`, `mongoose` ORM, `bcrypt`, `joi` for validating input data.  
Realised jwt authorisation.  
There are two different role in system: [Driver](#driver) and [Shipper](#shipper).
## Roles posibilities
### Driver
1. Driver is able to register in the system;
2. Driver is able to login into the system;
3. Driver is able to view his profile info;
4. Driver is able to change his account password;
5. Driver is able to add trucks;
6. Driver is able to view created trucks;
7. Driver is able to assign truck to himself;
8. Driver is able to update not assigned to him trucks info;
9. Driver is able to delete not assigned to him trucks;
10. Driver is able to view assigned to him load;
11. Driver is able to interact with assigned to him load;
### Shipper
1. Shipper is able to register in the system;
2. Shipper is able to login into the system;
3. Shipper is able to view his profile info;
4. Shipper is able to change his account password;
5. Shipper is able to delete his account;
6. Shipper is able to create loads in the system;
7. Shipper is able to view created loads;
8. Shipper is able to update loads with status ‘NEW';
9. Shipper is able to delete loads with status 'NEW';
10. Shipper is able to post a load;
11. Shipper is able to view shipping info;
## Install and start
1. Clone gitlab repo into your local one.
2. Run `npm i` to install dependencies.
3. Run `npm start` to start the server.

## Docs
Api docs [file](https://gitlab.com/voonya/nodejs3/-/tree/main/api-flow).<br>
Flow chars are [here](https://gitlab.com/voonya/nodejs3/-/tree/main/api-flow).

