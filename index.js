const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const mongoose = require('mongoose');
const router = require('./routers/index.js');
const disableCors = require('./middlewares/disable-cors.js');
const errorHandler = require('./middlewares/error-handler.js');
const {DB_URL, PORT} = require('./config.js');

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(morgan('common', {
  stream: fs.createWriteStream('./logs.log', {flags: 'a'}),
}));
app.use(morgan('dev'));

app.use('/api', router);
// add logger;
app.use(disableCors);
app.use(errorHandler);

const main = async () => {
  try {
    await mongoose.connect(DB_URL)
        .then(
            console.log('Connected to db!'),
        );
    app.listen(PORT || 8080, () => {
      console.log('Server started on port: ', PORT || 8080);
    });
  } catch (err) {
    console.log(err);
  }
};


main();
