const joi = require('joi');
const roles = require('./User/roles.js');
const truckTypes = require('./Truck/truck-types.js');
const loadStatus = require('../models/Load/load-status.js');

const passwordSchema = joi.string()
    .min(2)
    .max(30)
    .required();
const emailSchema = joi.string()
    .email()
    .required();
const roleSchema = joi.string()
    .valid(...Object.values(roles))
    .required();
const truckTypeSchema = joi.string()
    .valid(...Object.keys(truckTypes))
    .required();

const registerSchema = joi.object({
  email: emailSchema,
  password: passwordSchema,
  role: roleSchema,
});

const loginSchema = joi.object({
  email: emailSchema,
  password: passwordSchema,
});

const forgotPasswordSchema = joi.object({
  email: emailSchema,
});


const changePasswordSchema = joi.object({
  oldPassword: passwordSchema,
  newPassword: passwordSchema,

});

const createTruckSchema = joi.object({
  type: truckTypeSchema,
});

const idSchema = joi.string()
    .required();

const dimensionLoadSchema = joi.object({
  width: joi.number().required(),
  height: joi.number().required(),
  length: joi.number().required(),
});

const loadPayloadSchema = joi.object({
  name: joi.string()
      .min(1)
      .max(75)
      .required(),
  payload: joi.number().required(),
  pickup_address: joi.string()
      .min(1)
      .max(400)
      .required(),
  delivery_address: joi.string()
      .min(1)
      .max(400)
      .required(),
  dimensions: dimensionLoadSchema,
});

const filterLoads = joi.object({
  status: joi.string()
      .valid(...Object.values(loadStatus)),
  limit: joi.number().max(50),
  offset: joi.number(),
});
module.exports = {
  registerSchema,
  loginSchema,
  forgotPasswordSchema,
  changePasswordSchema,
  createTruckSchema,
  idSchema,
  truckTypeSchema,
  loadPayloadSchema,
  filterLoads,
};
