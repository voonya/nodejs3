
class UserDto {
    constructor(user) {
        this._id = user._id;
        this.email = user.email;
        this.role = user.role;
        this.created_date = user.created_date;
    }
}

module.exports = UserDto;