const { Schema } = require("mongoose");

class LoadDto {
        _id
        created_by
        assigned_to
        status
        state
        name
        pickup_address
        delivery_address
        dimensions
        payload
        created_date
        logs
    constructor(load) {
        this._id = load._id;
        this.created_by = load.created_by;
        this.assigned_to = load.assigned_to;
        this.status = load.status;
        this.state = load.state;
        this.name = load.name;
        this.pickup_address = load.pickup_address;
        this.delivery_address = load.delivery_address;
        this.dimensions = load.dimensions;
        this.payload = load.payload;
        this.created_date = load.created_date;
        this.logs = load.logs;
    }
}

module.exports = LoadDto;
