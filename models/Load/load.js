const {model, Schema} = require('mongoose');

const LoadSchema = new Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String, default: null},
  status: {type: String, required: true},
  state: {type: String, default: null},
  name: {type: String, required: true},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  dimensions: {
    width: {type: Number, required: true},
    length: {type: Number, required: true},
    height: {type: Number, required: true},
  },
  payload: {type: Number, required: true},
  created_date: {type: Date, required: true},
  logs: {type: Array, default: []},
});

module.exports = model('Load', LoadSchema);
