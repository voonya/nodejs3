
class TruckDto {
    _id;
    created_by;
    assigned_to;
    type;
    status;
    created_date;

    constructor(truck) {
            this._id = truck._id;
            this.created_by = truck.created_by;
            this.assigned_to = truck.assigned_to;
            this.type = truck.type;
            this.status = truck.status;
            this.created_date = truck.created_date;
    }
}

module.exports = TruckDto;