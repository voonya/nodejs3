
const trucksStatus = {
  IN_SERVICE: 'IS',
  ON_LOAD: 'OL',
};

module.exports = trucksStatus;
