const {model, Schema} = require('mongoose');

const TruckSchema = new Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String, required: false, default: null},
  status: {type: String, required: true},
  type: {type: String, required: true},
  created_date: {type: Date, required: true},
});

module.exports = model('Truck', TruckSchema);
