
const TruckTypes = {
  'SPRINTER': {
    size: {
      width: 170,
      length: 300,
      height: 250,
    },
    payload: 1700,
  },
  'SMALL STRAIGHT': {
    size: {
      width: 170,
      length: 500,
      height: 250,
    },
    payload: 2500,
  },
  'LARGE STRAIGHT': {
    size: {
      width: 200,
      length: 700,
      height: 350,
    },
    payload: 4000,
  },
};

module.exports = TruckTypes;
