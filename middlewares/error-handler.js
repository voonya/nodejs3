const ApiError = require('../exceptions/api-error.js');

function errorHandler(err, req, res, next) {
  console.log('Error:', err);
  if (err instanceof ApiError) {
    return res.status(err.status).json({
      message: err.message,
      errors: err.errors,
    });
  }
  return res.status(500).json({
    message: 'Server error!',
  });
}

module.exports = errorHandler;
