const UsersService = require('../services/users-service.js');
const UserRoles = require('../models/User/roles.js');
const UserService = require('../services/users-service');
const ApiError = require('../exceptions/api-error');

async function driverGuard(req, res, next) {
  const user = await UserService.getUser(req.user._id);
  if (user.role !== UserRoles.DRIVER) {
    return next(ApiError.badRequest(`Only user with role ${UserRoles.DRIVER} can do it!`));
  }
  next();
}

module.exports = driverGuard;
