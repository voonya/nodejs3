const UsersService = require('../services/users-service.js');
const UserRoles = require('../models/User/roles.js');
const UserService = require('../services/users-service');
const ApiError = require('../exceptions/api-error');

async function shipperGuard(req, res, next) {
  const user = await UserService.getUser(req.user._id);
  if (user.role !== UserRoles.SHIPPER) {
    return next(ApiError.badRequest(`Only user with role ${UserRoles.SHIPPER} can do it!`));
  }
  next();
}

module.exports = shipperGuard;
