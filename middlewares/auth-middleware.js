const ApiError = require('../exceptions/api-error.js');
const TokenService = require('../services/token-service.js');


function authGuard(req, res, next) {
  const accessToken = req.headers.authorization?.split(' ')[1];
  console.log(req.headers.authorization, accessToken);
  if (!accessToken) {
    return next(ApiError.Unauthorized('Jwt must be provided!'));
  }

  const user = TokenService.verifyAccessToken(accessToken);
  if (!user) {
    return next(ApiError.Unauthorized('Invalid jwt signature!'));
  }

  req.user = user;
  next();
}

module.exports = authGuard;
