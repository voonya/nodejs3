const LoadsService = require('../services/loads-service.js');
const Validator = require('../exceptions/validator.js');
const {loadPayloadSchema,
  filterLoads,
  idSchema} = require('../models/joi-schemas.js');

class LoadsController {
  async getUserLoads(req, res, next) {
    try {
      const userId = req.user._id;
      Validator.validate(req.query, filterLoads);
      const status = req.query.status;
      const limit = req.query.limit || 10;
      const offset = req.query.offset || 0;
      const loads = await LoadsService.getUserLoads(userId, status,
          limit, offset);
      return res.status(200).json({
        message: 'Success!',
        loads,
      });
    } catch (e) {
      next(e);
    }
  }
  async addLoad(req, res, next) {
    try {
      Validator.validate(req.body, loadPayloadSchema);
      const userId = req.user._id;
      await LoadsService.addLoad(userId, req.body);
      return res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
  async getActiveLoad(req, res, next) {
    try {
      const id = req.user._id;
      const load = await LoadsService.getActiveDriverLoad(id);
      return res.status(200).json({
        message: 'Success!',
        load,
      });
    } catch (e) {
      next(e);
    }
  }
  async setNextLoadState(req, res, next) {
    try {
      const id = req.user._id;
      await LoadsService.nextLoadState(id);
      return res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
  async getLoad(req, res, next) {
    try {
      const {id} = req.params;
      const userId = req.user._id;
      Validator.validate(id, idSchema);
      const load = await LoadsService.getLoad(id, userId);
      return res.status(200).json({
        message: 'Success!',
        load,
      });
    } catch (e) {
      next(e);
    }
  }
  async updateLoad(req, res, next) {
    try {
      const userId = req.user._id;
      const {id} = req.params;
      Validator.validate(id, idSchema);
      Validator.validate(req.body, loadPayloadSchema);
      await LoadsService.updateLoad(id, userId, req.body);
      return res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
  async deleteLoad(req, res, next) {
    try {
      const {id} = req.params;
      const userId = req.user._id;
      Validator.validate(id, idSchema);
      await LoadsService.deleteLoad(id, userId);
      return res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
  async postUserLoad(req, res, next) {
    try {
      const {id} = req.params;
      const userId = req.user._id;
      Validator.validate(id, idSchema);
      await LoadsService.postLoad(id, userId);
      return res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
  async getShippingInfo(req, res, next) {
    try {
      const {id} = req.params;
      const userId = req.user._id;
      Validator.validate(id, idSchema);
      const info = await LoadsService.getShippingInfo(id, userId);
      return res.status(200).json({
        message: 'Success!',
        ...info,
      });
    } catch (e) {
      next(e);
    }
  }
}


module.exports = new LoadsController();
