const Validator = require('../exceptions/validator.js');
const TruckService = require('../services/truck-service.js');
const {createTruckSchema, idSchema, truckTypeSchema} = require('../models/joi-schemas.js');
class TrucksController {
  async getTrucks(req, res, next) {
    try {
      const id = req.user._id;
      const trucks = await TruckService.getTrucksByUserId(id);
      res.status(200).json({
        message: 'Success!',
        trucks,
      });
    } catch (e) {
      next(e);
    }
  }
  async getTruck(req, res, next) {
    try {
      const {id} = req.params;
      const userId = req.user._id;
      Validator.validate(id, idSchema);
      const truck = await TruckService.getTruckById(id, userId);
      res.status(200).json({
        message: 'Success!',
        truck,
      });
    } catch (e) {
      next(e);
    }
  }
  async createTruck(req, res, next) {
    try {
      const id = req.user._id;
      Validator.validate(req.body, createTruckSchema);
      const {type} = req.body;
      await TruckService.createTruck(id, type);
      res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
  async updateTruck(req, res, next) {
    try {
      const {type} = req.body;
      const {id} = req.params;
      const userId = req.user._id;
      Validator.validate(type, truckTypeSchema);
      Validator.validate(id, idSchema);
      await TruckService.updateTruckInfo(id, userId, type);
      res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
  async assignTruck(req, res, next) {
    try {
      const {id} = req.params;
      const userId = req.user._id;
      Validator.validate(id, idSchema);

      await TruckService.assignedTruck(id, userId);
      res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
  async deleteTruck(req, res, next) {
    try {
      const {id} = req.params;
      const userId = req.user._id;
      Validator.validate(id, idSchema);
      await TruckService.deleteTruck(id, userId);
      res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new TrucksController();
