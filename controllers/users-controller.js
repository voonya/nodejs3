const UsersService = require('../services/users-service.js');
const Validator = require('../exceptions/validator.js');
const {changePasswordSchema} = require('../models/joi-schemas.js');
class UsersController {
  async getMe(req, res, next) {
    try {
      const id = req.user._id;
      const user = await UsersService.getUser(id);
      return res.status(200).json({
        message: 'Success!',
        user,
      });
    } catch (e) {
      next(e);
    }
  }
  async deleteMe(req, res, next) {
    try {
      const id = req.user._id;
      await UsersService.deleteUser(id);
      return res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
  async changePassword(req, res, next) {
    try {
      const id = req.user._id;
      Validator.validate(req.body, changePasswordSchema);
      const {oldPassword, newPassword} = req.body;
      await UsersService.changePassword(id, oldPassword, newPassword);
      return res.status(200).json({
        message: 'Success!',
      });
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new UsersController();
