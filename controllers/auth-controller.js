const AuthService = require('../services/auth-service.js');
const Validator = require('../exceptions/validator.js');

const {registerSchema,
  loginSchema,
  forgotPasswordSchema} = require('../models/joi-schemas.js');

class AuthController {
  async register(req, res, next) {
    try {
      Validator.validate(req.body, registerSchema);
      const {email, password, role} = req.body;
      await AuthService.register(email, password, role);
      return res.status(200).json({
        message: 'Successfully registered!',
      });
    } catch (e) {
      next(e);
    }
  }

  async login(req, res, next) {
    try {
      Validator.validate(req.body, loginSchema);
      const {email, password} = req.body;
      const accessToken = await AuthService.login(email, password);
      return res.status(200).json({
        message: 'Successfully logined!',
        jwt_token: accessToken,
      });
    } catch (e) {
      next(e);
    }
  }

  async forgotPassword(req, res, next) {
    try {
      Validator.validate(req.body, forgotPasswordSchema);
      await AuthService.forgotPassword(req.body.email);
      return res.status(200).json({
        message: 'New password sent to your email address',
      });
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new AuthController();
