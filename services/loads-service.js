const ApiError = require('../exceptions/api-error.js');
const LoadModel = require('../models/Load/load.js');
const LoadDto = require('../models/Load/load-dto.js');
const UserService = require('./users-service.js');
const TruckService = require('./truck-service.js');
const UserRoles = require('../models/User/roles.js');
const LoadStatus = require('../models/Load/load-status.js');
const LoadState = require('../models/Load/load-state.js');
const TruckStatus = require('../models/Truck/trucks-status.js');

class LoadsService {
  async addLoad(userId, payload) {
    await LoadModel.create({
      created_by: userId,
      status: LoadStatus.NEW,
      ...payload,
      created_date: new Date(),
      logs: [{
        message: 'Load created',
        time: new Date(),
      }],
    });
  }

  async getUserLoads(userId, status, limit, offset) {
    const user = await UserService.getUser(userId);
    if (!status) {
      if (user.role === UserRoles.SHIPPER) {
        let loads = await LoadModel.find({created_by: userId}, {}, {skip: offset, limit});
        loads = loads.map((load) => new LoadDto(load));
        return loads;
      }

      if (user.role === UserRoles.DRIVER) {
        let loads = await LoadModel.find({assigned_to: userId}, {}, {skip: offset, limit});
        loads = loads.map((load) => new LoadDto(load));
        return loads;
      }
      throw ApiError.badRequest('Specify role!');
    }

    if (user.role === UserRoles.SHIPPER) {
      let loads = await LoadModel.find({created_by: userId, status}, {}, {skip: offset, limit});
      loads = loads.map((load) => new LoadDto(load));
      return loads;
    }

    if (user.role === UserRoles.DRIVER) {
      let loads = await LoadModel.find({assigned_to: userId, status}, {}, {skip: offset, limit});
      loads = loads.map((load) => new LoadDto(load));
      return loads;
    }
    throw ApiError.badRequest('Specify role!');
  }

  async deleteLoad(id, userId) {
    const deleted = await LoadModel.deleteOne({_id: id, created_by: userId});
    if (deleted.deletedCount === 0) {
      throw ApiError.badRequest('User does not have a load with this id!');
    }
  }

  async getLoad(id, userId) {
    const load = await LoadModel.findOne({_id: id, created_by: userId});
    if (!load) {
      throw ApiError.badRequest('Load with this id does not exist!');
    }
    return new LoadDto(load);
  }

  async getShippingInfo(id, userId) {
    const load = await this.getLoad(id, userId);
    let truck = {};
    if (load.assigned_to) {
      truck = await TruckService.getAssignedTruckByUserId(load.assigned_to);
    }
    return {load, truck};
  }


  async getActiveDriverLoad(userId) {
    const load = await LoadModel.findOne({assigned_to: userId, status: {$eq: LoadStatus.ASSIGNED}});
    if (!load) {
      throw ApiError.badRequest('User does not have active load!');
    }
    return new LoadDto(load);
  }

  async nextLoadState(userId) {
    const load = await this.getActiveDriverLoad(userId);
    console.log(load);
    const index = LoadState.indexOf(load.state);
    if (index === LoadState.length - 1) {
      throw ApiError.badRequest('The final state already reached!');
    }

    if (index === LoadState.length - 2) {
      await LoadModel.updateOne({_id: load._id}, {
        $set: {
          status: LoadStatus.SHIPPED,
        },
      });
      const truck = await TruckService.getAssignedTruckByUserId(load.assigned_to);
      await TruckService.changeTruckStatus(truck._id, TruckStatus.IN_SERVICE);
    }
    await LoadModel.updateOne({_id: load._id}, {
      $set: {
        state: LoadState[index + 1],
      },
    });
    await this.log(load._id, `Change state of load from ${LoadState[index]} to ${LoadState[index + 1]}`);
  }

  async updateLoad(id, userId, payload) {
    const load = await LoadModel.findOne({_id: id, created_by: userId});
    if (!load) {
      throw ApiError.badRequest('User does not have load with this id!');
    }

    await LoadModel.updateOne({_id: id}, {$set: payload});
  }

  async postLoad(id, userId) {
    // set load status to POSTED
    const load = await LoadModel.findOne({_id: id, created_by: userId});
    if (!load) {
      throw ApiError.badRequest('User does not have load with this id!');
    }

    await LoadModel.updateOne({_id: id}, {
      $set: {
        status: LoadStatus.POSTED,
      },
    });
    await this.log(load._id, `Changed status from ${LoadStatus.NEW} to ${LoadStatus.POSTED}`);
    const truck = await TruckService.findSuitableTruck(load.payload, load.dimensions);
    if (!truck) {
      await LoadModel.updateOne({_id: id}, {
        $set: {
          status: LoadStatus.NEW,
        },
      });
      await this.log(load._id, `Did not find suitable car. Changed status from ${LoadStatus.POSTED} to ${LoadStatus.NEW}`);
      throw ApiError.badRequest('Can not find available truck with suitable parameters!');
    }

    // change truck status
    await TruckService.changeTruckStatus(truck._id, TruckStatus.ON_LOAD);
    await LoadModel.updateOne({_id: id}, {
      $set: {
        status: LoadStatus.ASSIGNED,
        state: LoadState[0],
        assigned_to: truck.assigned_to,
      },
    });
    await this.log(load._id, `Assigned load to driverId ${truck.assigned_to}.
                      Changed status from ${LoadStatus.POSTED} to ${LoadStatus.ASSIGNED}`);
  }

  async log(id, message) {
    const loadUpdate = await LoadModel.updateOne({_id: id}, {$push: {
      logs: {
        message,
        time: new Date(),
      },
    }});
    console.log(loadUpdate);
  }
}

module.exports = new LoadsService();
