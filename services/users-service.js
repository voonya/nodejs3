const UserModel = require('../models/User/user.js');
const UserDto = require('../models/User/user-dto.js');
const ApiError = require('../exceptions/api-error.js');
const bcrypt = require('bcrypt');
const {SALT} = require('../config');

class UsersService {
  async getUser(id) {
    console.log('Id:', id);
    const user = await UserModel.findOne({_id: id});
    if (!user) {
      throw ApiError.badRequest('User with this id does not exist!');
    }
    return new UserDto(user);
  }
  async deleteUser(id) {
    const result = await UserModel.deleteOne({_id: id});
    if (result.deletedCount === 0) {
      throw ApiError.badRequest('User with this credentials does not exist!');
    }
  }

  async changePassword(id, oldPassword, newPassword) {
    const user = await UserModel.findOne({_id: id});
    if (!user) {
      throw ApiError.badRequest('User with this credentials does not exist!');
    }

    const isPasswordsEqual = await bcrypt.compare(oldPassword, user.password);
    if (!isPasswordsEqual) {
      throw ApiError.badRequest('Incorrect password!');
    }

    await this.updatePassword(id, newPassword);
  }

  async updatePassword(id, password) {
    const user = await UserModel.findOne({_id: id});
    if (!user) {
      throw ApiError.badRequest('User with this credentials does not exist!');
    }
    const passwordHashed = await bcrypt.hash(password, SALT);
    await UserModel.updateOne({_id: id}, {$set: {password: passwordHashed}});
  }
}

module.exports = new UsersService();
