const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config.js');

class TokenService {
  generateAccessToken(payload, expiresIn = '1h') {
    const accessToken = jwt.sign(payload, JWT_SECRET, {expiresIn});
    return accessToken;
  }

  verifyAccessToken(token) {
    try {
      const userData = jwt.verify(token, JWT_SECRET);
      return userData;
    } catch (e) {
      return null;
    }
  }
}

module.exports = new TokenService();
