const bcrypt = require('bcrypt');
const UserModel = require('../models/User/user.js');
const ApiError = require('../exceptions/api-error.js');
const TokenService = require('./token-service.js');
const MailService = require('./mail-service.js');
const UsersService = require('./users-service.js');
const UserDto = require('../models/User/user-dto.js');
const {SALT} = require('../config.js');

class AuthService {
  async register(email, password, role) {
    const user = await UserModel.findOne({email});
    if (user) {
      throw ApiError.badRequest('User with this email already exists!');
    }

    const passwordHashed = await bcrypt.hash(password, SALT);

    const userAdded = await UserModel.create({
      email,
      password: passwordHashed,
      role,
      created_date: new Date(),
    });

    console.log('Added user: ', userAdded);
  }

  async login(email, password) {
    const user = await UserModel.findOne({email});
    if (!user) {
      throw ApiError.badRequest('User with this email does not exist!');
    }

    const isPasswordsEqual = await bcrypt.compare(password, user.password);
    if (!isPasswordsEqual) {
      throw ApiError.badRequest('Incorrect password!');
    }

    const userDto = new UserDto(user);
    return await TokenService.generateAccessToken({...userDto});
  }

  async forgotPassword(email) {
    const user = await UserModel.findOne({email});
    if (!user) {
      throw ApiError.badRequest('User with this email does not exist!');
    }

    const password = this.generatePassword();
    await UsersService.updatePassword(user._id, password);
    await MailService.sendMail(email, 'Changing password', `Your new password: ${password}`);
  }

  generatePassword() {
    const password = Math.random().toString(36).slice(-8) +
                        Math.random().toString(36).slice(-8);
    return password;
  }
}

module.exports = new AuthService();
