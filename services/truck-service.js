const TruckModel = require('../models/Truck/truck.js');
const TruckDto = require('../models/Truck/truck-dto.js');
const TruckStatus = require('../models/Truck/trucks-status.js');
const ApiError = require('../exceptions/api-error.js');
const TruckTypes = require('../models/Truck/truck-types.js');
class TruckService {
  async getTrucksByUserId(id) {
    let trucks = await TruckModel.find({created_by: id});
    trucks = trucks.map((truck) => new TruckDto(truck));
    console.log(trucks);
    return trucks;
  }
  async createTruck(userId, type) {
    await TruckModel.create({
      created_by: userId,
      status: TruckStatus.IN_SERVICE,
      type: type,
      created_date: new Date(),

    });
  }
  async getTruckById(id, userId) {
    const truck = await TruckModel.findOne({_id: id, created_by: userId});
    if (!truck) {
      throw ApiError.badRequest('User does not have truck with this id!');
    }
    return (new TruckDto(truck));
  }

  async updateTruckInfo(id, userId, type) {
    const truck = await TruckModel.findOne({_id: id, created_by: userId});
    if (!truck) {
      throw ApiError.badRequest('User does not have truck with this id!');
    }

    if (truck.assigned_to) {
      throw ApiError.badRequest('You can`t change info if truck is assigned!');
    }

    await TruckModel.updateOne({_id: id, created_by: userId}, {$set: {type}});
  }

  async deleteTruck(id, userId) {
    const result = await TruckModel.deleteOne({_id: id,
      created_by: userId,
      assigned_to: null});
    if (result.deletedCount === 0) {
      throw ApiError.badRequest(`User does not have truck with this 
                                          or it is already assigned!`);
    }
  }
  async assignedTruck(id, userId) {
    const assignedTruck = await TruckModel.findOne({assigned_to: userId});
    if (assignedTruck) {
      throw ApiError.badRequest('User already assigned truck!');
    }

    const truck = await TruckModel.findOne({_id: id, created_by: userId});
    if (!truck) {
      throw ApiError.badRequest('User does not have truck with this id!');
    }

    if (truck.assigned_to) {
      throw ApiError.badRequest('Truck is already assigned!');
    }

    await TruckModel.updateOne({_id: id, created_by: userId}, {$set: {assigned_to: userId}});
  }

  async getAssignedTruckByUserId(userId) {
    const truck = await TruckModel.findOne({assigned_to: userId});
    if (!truck) {
      throw ApiError.badRequest('User does not have truck with this id!');
    }
    return (new TruckDto(truck));
  }

  async changeTruckStatus(id, status) {
    const truck = await TruckModel.findOne({_id: id});
    if (!truck) {
      throw ApiError.badRequest('Truck with this id does not exist!');
    }
    await TruckModel.updateOne({_id: id}, {
      $set: {
        status,
      },
    });
  }

  async findSuitableTruck(payload, dimensions) {
    let trucks = await TruckModel.find({
      assigned_to: {$ne: null},
      status: TruckStatus.IN_SERVICE,
    });
    trucks = trucks.filter((truck) => this.isTruckSuit(TruckTypes[truck.type], payload, dimensions));
    trucks = trucks.sort((a, b) => {
      return a.payload - b.payload;
    });
    if (trucks.length === 0) {
      return null;
    }
    return trucks[0];
  }

  isTruckSuit(truck, payload, dimensions) {
    return (truck.payload >= payload &&
           truck.size.width >= dimensions.width &&
           truck.size.height >= dimensions.height &&
           truck.size.length >= dimensions.length
    );
  }
}

module.exports = new TruckService();
