const nodemailer = require('nodemailer');

const {MAIL_HOST, MAIL_PORT, MAIL_USERNAME, MAIL_PASSWORD} = require('../config.js');
class MailService {
    constructor() {
        this.transporter = nodemailer.createTransport({
            host: MAIL_HOST,
            port: MAIL_PORT,
            secure: true, // true for 465, false for other ports
            auth: {
                user: MAIL_USERNAME,
                pass: MAIL_PASSWORD,
            },
        })
    }
    async sendMail(email, subject, text){
        try{
            const info = await this.transporter.sendMail({
                to: email,
                subject,
                text
            })
            console.log(info);
        }
        catch (e) {
            throw (e);
        }

    }
}

module.exports = new MailService();