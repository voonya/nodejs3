const router = require('express').Router();
const authRouter = require('./auth-router.js');
const usersRouter = require('./users-router.js');
const trucksRouter = require('./trucks-router.js');
const loadsRouter = require('./loads-router.js');

const authGuard = require('../middlewares/auth-middleware.js');
router.use('/auth', authRouter);
router.use('/users', authGuard, usersRouter);
router.use('/trucks', authGuard, trucksRouter);
router.use('/loads', authGuard, loadsRouter);


module.exports = router;
