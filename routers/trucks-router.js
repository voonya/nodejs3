const trucksRouter = require('express').Router();
const TrucksController = require('../controllers/trucks-controller.js');
const driverGuard = require('../middlewares/driver-middleware.js');
trucksRouter.get('/', driverGuard, TrucksController.getTrucks);
trucksRouter.post('/', driverGuard, TrucksController.createTruck);
trucksRouter.get('/:id', driverGuard, TrucksController.getTruck);
trucksRouter.put('/:id', driverGuard, TrucksController.updateTruck);
trucksRouter.delete('/:id', driverGuard, TrucksController.deleteTruck);
trucksRouter.post('/:id/assign', driverGuard, TrucksController.assignTruck);


module.exports = trucksRouter;
