const loadsRouter = require('express').Router();
const LoadsController = require('../controllers/loads-controller.js');
const shipperGuard = require('../middlewares/shipper-middleware.js');
const driverGuard = require('../middlewares/driver-middleware.js');

loadsRouter.get('/', LoadsController.getUserLoads);
loadsRouter.post('/', shipperGuard, LoadsController.addLoad);
loadsRouter.get('/active', driverGuard, LoadsController.getActiveLoad);
loadsRouter.patch('/active/state', driverGuard, LoadsController.setNextLoadState);

loadsRouter.get('/:id', LoadsController.getLoad);
loadsRouter.put('/:id', shipperGuard, LoadsController.updateLoad);
loadsRouter.delete('/:id', shipperGuard, LoadsController.deleteLoad);
loadsRouter.post('/:id/post', LoadsController.postUserLoad);
loadsRouter.get('/:id/shipping_info', shipperGuard, LoadsController.getShippingInfo);

module.exports = loadsRouter;
