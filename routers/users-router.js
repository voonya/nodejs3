const usersRouter = require('express').Router();
const UsersController = require('../controllers/users-controller.js');

usersRouter.get('/me', UsersController.getMe);
usersRouter.delete('/me', UsersController.deleteMe);
usersRouter.patch('/me/password', UsersController.changePassword);


module.exports = usersRouter;
