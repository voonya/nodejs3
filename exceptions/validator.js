const ApiError = require('./api-error.js');

class Validator {
  static validate(data, schema) {
    const result = schema.validate(data);
    if (result.error) {
      throw ApiError.badRequest('Parameters are incorrect!', result.error);
    }
  }
}

module.exports = Validator;
