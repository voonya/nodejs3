class ApiError extends Error {
    constructor(status, message, errors = []) {
        super(message);
        this.status = status;
        this.errors = errors;
    }

    static Unauthorized(message){
        return new ApiError(401, message);
    }

    static badRequest(message, errors = []) {
        return new ApiError(400, message, errors)
    }
}

module.exports = ApiError;